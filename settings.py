import tkinter as tk


def settings():
    win = tk.Tk()

    label_1 = tk.Label(win, text='Hello!',
                       bg='red',
                       fg='white',
                       font=('Arial', 130, 'bold'),
                       # padx=20,
                       # pady=40,
                       width=5,  # ширина отступа от текста в символах
                       height=0,  # высота отступа от текста в символах
                       anchor='center',  # расположение текста по сторонам света
                       relief=tk.RAISED,
                       bd=20
                       )
    label_1.pack()

    Icon_photo = tk.PhotoImage(file='Enaena.png')
    win.iconphoto(False, Icon_photo)
    win.config(bg='#f5bdbd')  # цвет заднего фона окна

    win.title('Моё первое графическое приложение')
    win.geometry("850x600+600+200")  # расположение окна и его размер при запуске приложения
    win.resizable(True, True)
    win.minsize(400, 500)
    win.mainloop()
